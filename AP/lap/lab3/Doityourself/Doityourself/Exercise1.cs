﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Doityourself
{
    class Exercise1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=============================");

            Console.WriteLine("enter the atomic number you want: ");
            int n = int.Parse(Console.ReadLine());
            string[] symbol = new string[n];
            string[] fullName = new string[n];
            double[] atomicWeight = new double[n];
            int[] atomicNumber = new int[n];


            for (int i = 0; i < n; i++)
            {

                Console.Write("Enter atomic number: ");
                atomicNumber[i] = Convert.ToInt32(Console.ReadLine());
                if (atomicNumber[i] != 0)
                {
                    Console.Write("Enter symbol: ");
                    symbol[i] = Console.ReadLine();
                    Console.Write("Enter full name: ");
                    fullName[i] = Console.ReadLine();
                    Console.Write("Enter atomic weight: ");
                    atomicWeight[i] = Convert.ToDouble(Console.ReadLine());
                }
                else
                {
                    Console.WriteLine("No Sym Name Weight");
                    break;
                }


            }
            Console.WriteLine("-----------------------");
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"{atomicNumber[i]} {symbol[i]} {fullName[i]} {atomicWeight[i]}");
            }
            Console.ReadLine();
        }


    }
}

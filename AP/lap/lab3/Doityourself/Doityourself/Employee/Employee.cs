﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Doityourself.Employee
{
    class Employee
    {
        protected string fname;
        protected string lname;
        protected string address;
        protected long sin;
        protected double salary;

        public Employee(string fname, string lname, string address, long sin,double salary)
        {
            this.fname = fname;
            this.lname = lname;
            this.address = address;
            this.sin = sin;
            this.salary = salary;
        }
        protected Employee()
        {
            fname = "Nguyen van";
            lname = "A";
            address = "Ninh Binh";
            sin = 193456787;
            salary = 15.508;
        }

        public override string ToString()
        {
            return ("Employee : \n Name : " + fname + lname + "\n Address : " + address +
                "\n Sin : " + sin + "\n Salary : " + salary);
        }
        public double Bonus(double parameter)
        {
            return salary * parameter;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Window
    {
        protected int top;
        protected int left;
        //constructor takes two integers to(ham tao nhan hai so nguyen de)
        // fix location on the console(sua vi tri tren bang dieu khien)
        public Window(int top,int left)
        {
            this.top = top;
            this.left = left;
        }
        //simulates drawing the window(mô phỏng vẽ cửa sổ)
        public virtual void DrawWindow()
        {
            Console.WriteLine("Window: drawing Window at {0}, {1}",top,left);
        }
    }
    class ListBox : Window
    {
        //constructor adds a parameter(them tham so)
        public ListBox(int top,int left,string contents):base(top,left)//call base(cơ sở) constructor
        {
            listBoxContents = contents;
        }
        // an overridden version (note keyword) because in the// một phiên bản bị ghi đè (từ khóa ghi chú) vì trong
        // derived method we change the behavior// phương thức dẫn xuất chúng tôi thay đổi hành vi
        
        public override void DrawWindow()
        {
            base.DrawWindow(); // invoke(cầu khẩn : gọi) the base(cơ sở) method
            Console.WriteLine("Writing string to the listbox:{0}",listBoxContents);
        }
        private string listBoxContents;//new member variable
    }

    class Button : Window
    {
        public Button(int top,int left):base(top,left)
        {
        }
        //an overridden version (note keyword) because in the
        //derived method we change the behavior
        public override void DrawWindow()
        {
            Console.WriteLine("Drawing a button at {0}, {1}\n", top, left);
        }
    }

    class Polymorphism
    {
        static void Main(string[] args)
        {
            Window win = new Window(1, 2);
            ListBox lb = new ListBox(3, 4, "stand alone list box");
            Button b = new Button(5, 6);
            win.DrawWindow();
            lb.DrawWindow();
            b.DrawWindow();
            Window[] winArray = new Window[3];
            winArray[0] = new Window(1, 2);
            winArray[1] = new ListBox(3, 4,"List box in array");
            winArray[2] = new Button(5, 6);
            for(int i = 0; i < 3; i++)
            {
                winArray[i].DrawWindow();
            }
            Console.ReadLine();

        }
    }
}

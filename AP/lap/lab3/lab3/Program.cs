﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            //khai báo một tham chiếu đối tượng Car name myCar
            Car myCar;
            //tạo một đối tượng Car và gán địa chỉ của nó cho myCar
            Console.WriteLine("Creating a Car object and assigning " + "its memory location to mycar");
            myCar = new Car();

            //gán giá trị cho các trường của đối tượng Car bằng cách sử dụng myCar
            myCar.make = "Toyota";
            myCar.model = "MR2";
            myCar.color = "black";
            myCar.yearBuilt = 1995;

            //display the field values using myCar
            Console.WriteLine("myCar details:");
            Console.WriteLine("myCar.make= " + myCar.make);
            Console.WriteLine("myCar.model= " + myCar.model);
            Console.WriteLine("myCar.color= " + myCar.color);
            Console.WriteLine("myCar.yearBuilt= " + myCar.yearBuilt);

            //call the method using(sử dụng) myCar 
            myCar.Start();
            myCar.Stop();

            //declare another Car object reference and
            //create another(nữa) Car object
            Console.WriteLine("Creating another car object and" + "assigning its memory location to redPorsche");
            Car redPorsche = new Car();
            redPorsche.make = "Porsche";
            redPorsche.model = "Boxster";
            redPorsche.color = "red";
            redPorsche.yearBuilt = 2000;
            Console.WriteLine("redPorsche is a " + redPorsche.model);

            //change the object referenced by the myCar object // reference to the object reference by redPorshe
            Console.WriteLine("Assigning redPorsche to myCar");
            myCar = redPorsche;
            Console.WriteLine("myCar details: ");
            Console.WriteLine("myCar.make = " + myCar.make);
            Console.WriteLine("myCar.model = " + myCar.model);
            Console.WriteLine("myCar.color = " + myCar.color);
            Console.WriteLine("myCar.yearBuilt = " + myCar.yearBuilt);

            //assign null to myCar (myCar will no longer reference
            //an object)
            myCar = null;
            Console.ReadLine();






        }
    }


    class Car
    {
        public string make;
        public string model;
        public string color;
        public int yearBuilt;

        public void Start()
        {
            Console.WriteLine(model + "started");
        }
        public void Stop()
        {
            Console.WriteLine(model + "stopped");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class ForEach
    {
        static void Main(string[] args)
        {
            DateTime now = DateTime.Now;
            Random rand = new Random((int)now.Millisecond);
            int[] arr = new int[10];
            for(int i = 0; i < arr.Length; i++)
            {
                arr[i] = Convert.ToInt32(Console.ReadLine());
            }
            int total = 0;
            Console.WriteLine("Array values are ");
            foreach(int val in arr)
            {
                total += val;
                Console.WriteLine(val + ", ");
            }
            Console.WriteLine("\nAnd the average is {0,0:F1}",(double)total/(double)arr.Length);
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace FinallyStmt
{
    class FinallyDemo
    {
        static void Main(string[] args)
        {
            FileStream outStream = null;
            FileStream inStream = null;
            try
            {
                //mo file de ghi du lieu
                outStream = File.OpenWrite("DestinationFile.txt");
                //mở file để đọc dữ liệu
                inStream = File.OpenRead("BogusInputFile.txt");
                //các câu lệnh đọc dữ liệu từ file
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (outStream != null)
                {
                    outStream.Close();
                    Console.WriteLine("outStream closed");
                }
                if(inStream != null)
                {
                    inStream.Close();
                    Console.WriteLine("inStream closed");
                }
            }
        }
    }
}

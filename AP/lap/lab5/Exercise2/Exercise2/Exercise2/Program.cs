﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    public class InvalidSalary : ApplicationException
    {
        public InvalidSalary() : base("Senior lecturers receive a salary of over 60,000"){}

    }
    public class InvalidBonus : ApplicationException
    {
        public InvalidBonus() : base("Bonus is over 10,000 ") { }
    }


}

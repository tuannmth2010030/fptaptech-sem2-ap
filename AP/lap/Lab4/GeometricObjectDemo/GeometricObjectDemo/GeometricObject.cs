﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeometricObjectDemo
{
    public abstract class GeometricObject
    {
        protected string color;
        protected double weight;
        //contructor ma dinh
        protected GeometricObject()
        {
            color = "white";
            weight = 1.0;
        }
        //constructor a geometric object

        protected GeometricObject(string color, double weight)
        {
            this.color = color;
            this.weight = weight;
        }
        //properties
        public string PColor
        {
            get { return color; }
            set { color = value; }

        }
        public double PWeight
        {
            get { return weight; }
            set { weight = value; }
        }
        //abstract method
        public abstract double findArea();
        //abstract method
        public abstract double findPerimeter();
    }
}

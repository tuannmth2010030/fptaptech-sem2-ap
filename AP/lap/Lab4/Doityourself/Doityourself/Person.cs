﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Doityourself
{
    public abstract class Person
    {
        protected string pName;
        protected string phoneNo;
        protected string email;

        protected Person(string name,string phone,string email)
        {
            this.pName = name;
            this.phoneNo = phone;
            this.email = email;
        }
        protected Person()
        {
            pName = "NT";
            phoneNo = "454538564583";
            email = "mndf@gmail.com";
        }
        public string PName
        {
            get { return pName; }
            set { pName = value; }
        }
        public string PPhoneNo
        {
            get { return phoneNo; }
            set { phoneNo = value; }
        }

        public string PEmail
        {
            get { return email; }
            set { email = value; }
        }



    }
}

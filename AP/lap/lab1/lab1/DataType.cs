﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class DataType
    {
        static void Main(string[] args)
        {
            int intVal = 10;
            double dblVal = 3.142;
            string strVal = "Fpt Aptech";
            Console.WriteLine("{0} is an integer value", intVal);
            Console.WriteLine("{0} is an double value", dblVal);
            Console.WriteLine("{0} is an string ", strVal);
            Console.ReadLine();
            
        }
    }
}

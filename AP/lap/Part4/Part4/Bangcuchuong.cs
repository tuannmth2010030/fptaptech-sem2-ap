﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part4
{
    class Bangcuchuong
    {
        static void Main(string[] args)
        {   int n;
            do
            {
                Console.Write("Enter n: ");
                n = Convert.ToInt32(Console.ReadLine());
                for (int i = 1; i <= 9; i++)
                {
                    Console.WriteLine("{0} * {1} = {2} ", n, i, n * i);
                }
            } while (n == 0);
            
            Console.ReadLine();
        }
    }
}

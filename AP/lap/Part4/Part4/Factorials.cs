﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part4
{
    class Factorials
    {
        static void Main(string[] args)
        {
            do
            {
                Console.WriteLine("Enter n: ");
                int n = Convert.ToInt32(Console.ReadLine());
                int factorials = 1;
                for (int i = 1; i <= n; i++)
                {
                    factorials *= i;
                }
                Console.WriteLine("{0}! = {1}", n, factorials);

            } while (true);
           
            Console.ReadLine();
        }
    }
}

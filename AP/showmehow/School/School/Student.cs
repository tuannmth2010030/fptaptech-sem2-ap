﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School
{
    class Student
    {
        static void Main(string[] args)
        {
            //Declaring and intialising variables to store student details
            /*  int studentID = 1;
              string studentName = "David George";
              byte age = 18;
              char gender = 'M';
              float persent = 75.50F;
              bool pass = true;
              Console.WriteLine("Student ID: {0}", studentID);
              Console.WriteLine("Student Name: {0}",studentName);
              Console.WriteLine("Age: " + age);
              Console.WriteLine("Gender:" + gender);
              Console.WriteLine("Percentage: (0:F2)"+ persent);
              Console.WriteLine("Passed: {0}", pass);
            */
            //declaring integer constant to store value 100
            const int percentConst = 100;
            //Declaring variable to store the student name
            string studentName;
            //Declaring variable to store the student marks
            int english, maths, science;
            //Declaring and initialising variable to store the percentage
            float percent = 0.0F;
            Console.Write("Enter name of the student: ");
            studentName = Console.ReadLine();
            Console.Write("Enter marks for english: ");
            english = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter marks for maths: ");
            maths = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter marks for science: ");
            science = Convert.ToInt32(Console.ReadLine());

            //Calculating the percentage
            percent = ((english + maths + science) * percentConst) / 300;
            Console.WriteLine();
            Console.WriteLine("***Student Details***");
            Console.WriteLine("Student Name: "+studentName);
            Console.WriteLine("Marks obtained in English : {0}",english);
            Console.WriteLine("Marks obtained in Maths: {0}",maths);
            Console.WriteLine("Marks obtained in Science : {0}",science);
            Console.WriteLine("Percent : {0:F2}",percent);



        }
    }
}

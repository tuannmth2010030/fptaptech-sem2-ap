﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    class WorkerTest
    {
        static void Main(string[] args)
        {
            Worker objWorkers = new Worker();
            if (objWorkers.InputDetails())
            {
                //invoking the displayDetails method of the worker
                //class to display the details
                objWorkers.DisplayDetails();
            }
        }
        
    }
}

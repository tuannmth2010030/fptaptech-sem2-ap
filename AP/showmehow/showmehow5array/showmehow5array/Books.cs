﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace showmehow5array
{
    class Books
    {
        static void Main(string[] args)
        {
            //Array of string to store colum name;
            string[] colName = new string[4] { "Book Title", "Author", "Publisher", "Price($)" };

            //Two dimensional array of string type to store book details
            string[,] bookDetails = new string[2,4];

            //Accepting and storing the details of books
            Console.WriteLine("Enter book details: \n");
            for(int i = 0; i < bookDetails.GetLength(0); i++)
            {
                for(int j = 0; j < 4; j++)
                {
                    Console.Write("{0}: ",colName[j]);
                    bookDetails[i, j] = Console.ReadLine();
                }
                Console.WriteLine();
            }
            //Displaying the colum names
            Console.WriteLine("Details of books: \n");
            foreach(string names in colName)
            {
                Console.Write("{0}\t\t",names);
            }
            Console.WriteLine();
            //Displaying the details of books
            for(int i = 0; i < bookDetails.GetLength(0); i++)
            {
                for(int j = 0; j < 4; j++)
                {
                    Console.Write(bookDetails[i,j] + "\t\t");
                }
                Console.WriteLine();
            }

        }
    }
}

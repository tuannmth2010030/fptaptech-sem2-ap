﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace showmehow5array
{
    class Authors
    {
        static void Main(string[] args)
        {
            //Creating an object of array class for storing author IDs of type integer
            Array objAuthorID = Array.CreateInstance(typeof(int), 3);

            //Creating objects of Array class for storing author name,address, and state name of type string
            Array objAuthourNames = Array.CreateInstance(typeof(string), 3);
            Array objAddress = Array.CreateInstance(typeof(string), 3);
            Array objState = Array.CreateInstance(typeof(string), 3);

            //string variable to store user inputs
            string input = "";

            //Accepting the details of authors using the SetValue() method
            for(int i = 0; i < objAuthourNames.Length; i++)
            {
                Console.Write("Enter the ID of author: ");
                input = Console.ReadLine();
                objAuthorID.SetValue(Convert.ToInt32(input), i);
                Console.Write("Enter the name of author: ");
                input = Console.ReadLine();
                objAuthourNames.SetValue(input, i);
                Console.Write("Enter the address: ");
                input = Console.ReadLine();
                objAddress.SetValue(input, i);
                Console.Write("Enter the name of state: ");
                input = Console.ReadLine();
                objState.SetValue(input, i);
                Console.WriteLine();
            }

            //Displaying the details of authors using the GetValue() method
            Console.WriteLine("\nDetails of the authors : \n");
            Console.WriteLine("Author ID\tName\t\tAddress\t\t\tState");
            for(int i = 0; i <= objAuthourNames.GetUpperBound(0); i++)
            {
                Console.Write(objAuthorID.GetValue(i) + "\t\t");
                Console.Write(objAuthourNames.GetValue(i) + "\t");
                Console.Write(objAddress.GetValue(i) + "\t");
                Console.Write(objAuthorID.GetValue(i));
            }

            //sorting the names of authors the sort || method
            Array.Sort(objAuthourNames);

            //displaying the names of authors after sorting
            Console.WriteLine("\nAuthor after sorting : \n");
            for(int i=0;i<= objAuthourNames.GetUpperBound(0); i++)
            {
                Console.WriteLine(objAuthourNames.GetValue(i) + "\t");
            }


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace showmehow5array
{
    class Program
    {
        static void Main(string[] args)
        {
            //array of string to store the book title
            string[] bookTitle = new string[6];

            //Accepting and storeing the title of the books
            for(int i = 0; i < bookTitle.Length; i++)
            {
                Console.Write("Enter the title of book: ");
                bookTitle[i] = ReadLine();
                WriteLine();
            }

            //displaying the title of the books
            WriteLine("List of Book: ");
            foreach(string title in bookTitle)
            {
                WriteLine(title);
            }
            //Changning the title of fourth book
            bookTitle[3] = "Planet of the Apes";
            WriteLine("Newly changed title of book 4: " + bookTitle[3]);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Showmehow3
{
    class Product
    {
        static void Main(string[] args)
        {
            //Creating objects of object class to store the 
            //details of the product
            Object objProductId;
            Object objProductName;
            Object objPrice;
            Object objQuantity;

            Console.WriteLine("Enter the id of product: ");
            objProductId = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the name of product: ");
            objProductName = Console.ReadLine();

            Console.WriteLine("Enter price: ");
            objPrice = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Enter quantity: ");
            objQuantity = Convert.ToInt32(Console.ReadLine());


            //int productId = objProductId;

            //Concerting objects into their relevant types
            //using explicit unboxing

            int productId = (int)objProductId;
            string productName = (string)objProductName;
            double price = (double)objPrice;
            int quantity = (int)objQuantity;
            double amtPayable = (int)objQuantity * price;

            //displaying the details of theproduct
            Console.WriteLine("\nProduct Details: ");
            Console.WriteLine("Product Id: " + productId);
            Console.WriteLine("Product Name: " + productName);
            Console.WriteLine("Price: $" + price);
            Console.WriteLine("Quantity: " + quantity);
            Console.WriteLine("Amt Payable {0:F2}: ", amtPayable);
             
        }
    }
}

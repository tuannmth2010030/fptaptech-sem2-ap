﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Showmehow3
{
    class Employee
    {
        static void Main(string[] args)
        {
            int employeeId;
            string employeeName;
            string designation;
            float taxAmount = 0;
            double salary = 0, netSalary = 0;

            //Accepting the employee details using expliciting
            Console.WriteLine("Enter the id of an employee: ");
            employeeId = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the name of an employee: ");
            employeeName = Console.ReadLine();

            Console.WriteLine("Enter desigmation: ");
            designation = Console.ReadLine();

            Console.WriteLine("Enter salary: ");
            salary = Convert.ToDouble(Console.ReadLine());

            //calculating tax and net salary based on basic salary
            //using explicit and implicit typecasting
            if(salary >= 10000)
            {
                taxAmount = (float)(salary * 32.5 / 100);
            }
            else
            {
                taxAmount = (float)(salary * 24.8 / 100);
            }
            netSalary = salary - taxAmount;

            //displaying the details the employee using explicit typecasting
            Console.WriteLine("\nEmployee Details:");
            Console.WriteLine("Employee Id: " + employeeId);
            Console.WriteLine("Employee Name: " + employeeName);
            Console.WriteLine("Designarion: " + designation);
            Console.WriteLine("Salary : {0} $", salary);
            Console.WriteLine("Net Salary: {0:F2} $ is rounded off to {1} $", netSalary, netSalary);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Showmehow6ClassAndMethod
{
    class ProductTest
    {
        static void Main(string[] args)
        {
            //tạo ra một đối tượng của lớp product và gọi constructor mặc ddingj không tham số
            Product objProduct = new Product();
            objProduct.DisplayDetails();
            Console.WriteLine();

            //tạo ra một đối tượng của lớn product và gọi contructor tham số duy nhất
            Product objProduct1 = new Product(103);
            objProduct1.DisplayDetails();
            Console.WriteLine();

            //tạo ra một đối tượng của lớn product và gọi contructor với 4 tham số
            Product objProduct2 = new Product(113, "Television", 5660.45f, 68);
            objProduct2.DisplayDetails();
        }
    }
}

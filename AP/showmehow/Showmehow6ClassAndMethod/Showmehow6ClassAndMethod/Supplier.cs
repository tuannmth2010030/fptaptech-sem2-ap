﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Showmehow6ClassAndMethod
{
    class Supplier
    {
        /// <summary>
        /// Integer field to store the id ò the supplier
        /// </summary>
         
        private int _supplierID;
        ///<summary>
        ///string field to store the name of the supplier
        ///</summary>

        private string _supplierName;
        ///<summary>
        ///string field to store the city of the supplier
        /// </summary>

        private string _city;
        ///<summary>
        ///string field to store the phone number of the supplier
        /// </summary>
        private string _phoneNo;

        ///<summary>
        ///String field to store the email address of the supplier
        /// </summary>

        private string _email;
        ///<summary>
        ///Nethod to accept the details of the supplier
        /// </summary>
    
        internal void AcceptDetails()
        {
            Console.Write("Enter the ID of supplier: ");
            _supplierID = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Name of supplier: ");
            _supplierName = Console.ReadLine();

            Console.Write("Enter the name of City: ");
            _city = Console.ReadLine();

            Console.Write("Enter phone  No: ");
            _phoneNo = Console.ReadLine();

            Console.Write("Enter email address: ");
            _email = Console.ReadLine();
        }
        internal void DisplayDetails(int supplierID)
        {
            Console.WriteLine("Supplier ID: "+ _supplierID);
            
        }

        internal void DisplayDetails(string supplierName)
        {
            Console.WriteLine("Supplier Name: "+_supplierName);
        }
        internal void DisplayDetails(int supplierID,string supplierName)
        {
            Console.WriteLine($"\nSupplier {supplierName} with Id {supplierID} lives in city {_city}");

        }
    
    }
}

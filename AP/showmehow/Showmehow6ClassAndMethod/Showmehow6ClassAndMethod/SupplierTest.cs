﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Showmehow6ClassAndMethod
{
    class SupplierTest
    {
        static void Main()
        {
            //Creating an object of the supplier class 
            Supplier objSupplier = new Supplier();

            //Invoking the AcceptDetails method of the Supplier class
            //to accept the details of the supplier
            objSupplier.AcceptDetails();

            
            //integer variable to accept the id of the supplier
            int id = 0;

            //string variable to accept the name of the supplier
            string name = "";

            //accepting the id of the supplier
            Console.Write("\nEnter the id of the supplier: ");
            id = int.Parse(Console.ReadLine());

            //Invoking the DisplayDetails method of the Supplier class
            //to display the details of the supplier
            objSupplier.DisplayDetails(id);

            //Accepting the name of the supplier
            Console.Write("\nEnter the name of the supplier: ");
            name = Console.ReadLine();

            //Invoking the displayDetails method of the supplier class
            //to display the details of the supplier by passing name as a parameter
            objSupplier.DisplayDetails(name);

            //Involing the displayDetails method of the suppliet class
            //to display the city of the supplier by passing id name as parameter
            objSupplier.DisplayDetails(id, name);





        }
    }
}

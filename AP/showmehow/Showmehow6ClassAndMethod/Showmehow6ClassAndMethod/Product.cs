﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Showmehow6ClassAndMethod
{
    class Product
    {
        private int _productId;
        private string _productName;
        private float _price;
        private int _stock;

        //Constructor without parameters to initialise details of the product
        public Product()
        {
            _productId = 101;
            _productName = "Refrigerator";
            _price = 420.5f;
            _stock = 30;
        }

        //Constructor with a single parameter to initialise details of the product
        public Product(int id)
        {
            _productId = id;
            _productName = "Mashing Machine";
            _price = 677.3f;
            _stock = 25;
        }

        public Product(int id,string name,float price,int stock)
        {
            _productId = id;
            _productName = name;
            _price = price;
            _stock = stock;
        }

        public void DisplayDetails()
        {
            Console.WriteLine("Product Details: ");
            Console.WriteLine("Product Id: " + _productId);
            Console.WriteLine("Product name: "+ _productName);
            Console.WriteLine("Price: "  + _price);
            Console.WriteLine("Quantity in stock: "+ _stock);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_11_9
{
    class ManageProduct
    {
        public List<Product> listProduct = null;
        public ManageProduct()
        {
            listProduct = new List<Product>();
        }
        public int CountProduct()
        {
            int Count = 0;
            if (listProduct != null)
            {
                Count = listProduct.Count;
            }
            return Count;
        }

        //them moi san pham
        public void CreateProduct()
        {
            Product product = new Product();
            Console.Write("Nhap ma san pham: ");
            product.ProductId = Convert.ToString(Console.ReadLine());
            Console.Write("Nhap ten san pham: ");
            product.Name = Convert.ToString(Console.ReadLine());
            Console.Write("Nhap gia san pham: ");
            product.Price = Convert.ToDouble(Console.ReadLine());
            listProduct.Add(product);
            
        }
        public Product FindByProductId(string prodId)
        {
            Product searchId = null;
            if (listProduct != null && listProduct.Count > 0)
            {
                foreach(Product prod in listProduct)
                {
                    if (prod.ProductId == prodId)
                    {
                        searchId = prod;
                    }
                }
            }
            return searchId;
        }
        public bool DeleteProduct(string id)
        {
            bool deleteProduct = false;
            Product productId = FindByProductId(id);
            if (productId != null)
            {
                deleteProduct = listProduct.Remove(productId);
            }
            return deleteProduct;
        }
        //hien thi product
        public void ShowProduct(List<Product> products)
        {
            Console.WriteLine("{0,-5} | {1,-15} | {2;-25} |","ProductID","Name","Price");
            if(products != null && products.Count > 0)
            {
                foreach(Product pr in products)
                {
                    Console.WriteLine("{0,-5} | {1,-15} | {2;-25} |", pr.ProductId, pr.Name, pr.Price);
                }

            }
            Console.WriteLine();
        }

        //ham tra ve product
        public List<Product> GetProduct()
        {
            return listProduct;
        }
    }
}

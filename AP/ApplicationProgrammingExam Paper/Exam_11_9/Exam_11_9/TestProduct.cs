using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_11_9
{
    class TestProduct
    {
        ManageProduct mprod = new ManageProduct();
        public static void Main(string[] args)
        {
            TestProduct testProduct = new TestProduct();
            testProduct.menuProduct();
        }
        public void menuProduct()
        {
            int choice;
            while (true)
            {
                Console.WriteLine("************MENU***********");
                Console.WriteLine(" 1. Add product records");
                Console.WriteLine(" 2. Display product records");
                Console.WriteLine(" 3. Delete product by Id");
                Console.WriteLine(" 4. Exit");
                Console.WriteLine(" #Chon: ");
                try
                {
                    choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            mprod.CreateProduct();
                            Console.WriteLine("Ban co muon tao them san pham ? Y/N: ");
                            string ans = Console.ReadLine();
                            if (ans == "Y")
                            {
                                goto case 1;
                            }else if(ans == "N")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Khong hop le!!!");
                                
                            }
                            break;
                        case 2:
                            if (mprod.CountProduct() > 0)
                            {
                                Console.WriteLine("Xem san pham");
                                mprod.ShowProduct(mprod.GetProduct());
                            }
                            break;
                        case 3:
                            if (mprod.CountProduct() > 0)
                            {
                                string id;
                                Console.WriteLine("Xoa cau hoi");
                                Console.Write("Nhap ma san pham de xoa: ");
                                id = Console.ReadLine();
                                if (mprod.DeleteProduct(id))
                                {
                                    Console.WriteLine($"San pham co ma = {id} da duoc xoa");
                                }
                                else
                                {
                                    Console.WriteLine("San pham khong ton tai hoac da bi xoa  !!!");
                                }
                            }
                            break;
                        case 4:
                            Console.WriteLine("Bye!!!");
                            break;
                        default:
                            Console.WriteLine("Invalid!!!");
                            break;
                    }
                }catch(Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine(ex.Message);
                }
                
            }
        }
    }
}

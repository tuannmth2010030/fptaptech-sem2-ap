﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace showmehow4//bank
{
    class Employee
    {
        static void Main(string[] args)
        {
            //integer variable to store the employee id
            int employeeId;

            //string variables store the employee name and desigmation 
            string employeeName;
            string designation = "";

            //DateTime variable to store the employee's date of birth
            DateTime birthDate;

            //character variable to store the employee gender
            char gender;

            //Byte variable to store the number of years served
            byte yearsServed;

            //double variable to store bonus ,salary, tax amount, and net salary
            double bonus = 0, salary = 0, taxAmount = 0, netSalary = 0;

            //Accepting the details of the employee and validating it using if statement
            Console.Write("Enter the ID of employee: ");
            employeeId = Convert.ToInt32(Console.ReadLine());
            if (employeeId > 0)
            {
                Console.Write("Enter the name of employee: ");
                employeeName = Console.ReadLine();
                
                if(employeeName != "" && employeeName.Length < 40)
                {
                    Console.Write("Enter date of birth (MM/DD/YYYY): ");
                    birthDate = Convert.ToDateTime(Console.ReadLine());
                    int age = (DateTime.Today.Subtract(birthDate)).Days / 365;
                    if (age >= 18)
                    {
                        Console.Write("Enter gender (M/F): ");
                        gender = Convert.ToChar(Console.ReadLine());

                        Console.WriteLine("Select desigmation (choose the number) : ");
                        Console.WriteLine("1.Manager \n2.System Analyst \n3.Developer \n4.Accountant");
                        Console.Write("Enter your choice: ");
                        int choice = Convert.ToInt32(Console.ReadLine());

                        Console.Write("Enter the tenure in years: ");
                        yearsServed = Convert.ToByte(Console.ReadLine());

                        //Assigning salary based on the designation selected using swich statement
                        switch(choice){
                            case 1:
                                designation = "Manager";
                                salary = 21346;
                                break;
                            case 2:
                                designation = "System Analyst";
                                salary = 16729;
                                break;
                            case 3:
                                designation = "Developer";
                                salary = 14525;
                                break;
                            case 4:
                                designation = "Accountant";
                                salary = 13215;
                                break;
                        }
                        if (yearsServed >= 3)
                        {
                            if (salary > 20000)
                            {
                                bonus = salary * 0.09;
                            }else if(salary>14000 && salary <= 20000)
                            {
                                bonus=salary*0.05;
                            }
                            else
                            {
                                bonus = salary * 0.02;
                            }
                        }
                        taxAmount = salary * 33 / 100;
                        netSalary = salary - taxAmount;

                        //Displaying the details of employee Console.Write("\nEmployee Details:");
                        Console.WriteLine("\nEmployee Id: " + employeeId);
                        Console.WriteLine("Employee Name: " + employeeName);
                        Console.WriteLine("Date of birth: " + birthDate);
                        if (gender == 'M')
                        {
                            Console.WriteLine("Gender: Male");
                        }
                        else
                        {
                            Console.WriteLine("Gender: Female");
                        }
                        Console.WriteLine("Desigmation: "+ designation);
                        Console.WriteLine("Tenure: " + yearsServed);
                        Console.WriteLine("Salary: {0} $", salary);
                        Console.WriteLine("Tax Amount: {0} $", taxAmount);
                        Console.WriteLine("Net Salary: {0:F2} $ is rounded off to: {1} $", netSalary, (int)netSalary);
                        Console.WriteLine("Bonus: {0} $", bonus);
                    }
                    else
                    {
                        Console.WriteLine("Invalid entry for date of birth");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid entry for employee name");
                }
            }
            else
            {
                Console.WriteLine("Invalid entry for employee Id");
            }

            Console.ReadLine();
        }
    }
}

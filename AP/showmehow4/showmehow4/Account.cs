﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace showmehow4
{
    class Account
    {
        static void Main(string[] args)
        {
            //Array of int to store customer Id and account number
            int[] customerId = new int[3];
            int[] accountNumber = new int[3];

            //Array of string to store account type
            string[] accountType = new string[3];

            //Array of datetime to store the date on which the accout is opened
            DateTime[] dateOpened = new DateTime[3];

            //Array of double to store deposit and balance
            double[] deposi = new double[3];
            double[] balance = new double[3];

            //Integer variable to store the account number entered
            int choice = 0;
            int i = 0;
            bool found = false;

        //Accepting the details of account using goto and return statements
        accept:
            {
                Console.Write("Enter the id of customer: ");
                customerId[i] = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter account number: ");
                accountNumber[i] = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter account type [Fixed/Savings]: (F/S) ");
                accountType[i] = Console.ReadLine();
                Console.Write("Enter the date of opening account (MM/DD/YYYY): ");
                dateOpened[i] = Convert.ToDateTime(Console.ReadLine());
                Console.Write("Enter the amount deposited: ");
                deposi[i] = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine();
                if(deposi[i] <= 0)
                {
                    Console.WriteLine("Invalid Date Entry");
                    return;
                }
                else
                {
                    //calculating the balance
                    balance[i] = balance[i] + deposi[i];
                }
                i++;
            }
            if (i < customerId.Length)
            {
                goto accept;
            }

            //Displaying the account details
            Console.WriteLine("Account Details: \n");
            Console.WriteLine("Id\tA/c Type Opening Date\tDeposit($)    Balance($)");
            for (i = 0; i < customerId.Length; i++)
            {
                Console.Write("{0}\t{1} \t", customerId[i], accountNumber[i]);
                Console.Write("{0}\t   {1}\t", accountType[i], dateOpened[i].ToShortDateString());
                Console.WriteLine("{0}\t        {1}", deposi[i], balance[i]);
            }

            //Displaying the details of all except those that were created today
            Console.WriteLine("\n\nAccount Details: \n");
            Console.WriteLine("Id\tA/c No.\tA/c Type Opening Date\tDeposit($)     Balance($)");
            DateTime today = DateTime.Today.Date;
            for (i = 0; i < customerId.Length; i++)
            {
                //calculate the difference between dates
                int num = (today.Subtract(dateOpened[i])).Days;
                if (num == 0)
                {
                    continue;
                }
                else
                {
                    Console.Write("{0}\t{1} \t", customerId[i], accountNumber[i]);
                    Console.Write("{0}\t  {1}\t", accountType[i], dateOpened[i].ToShortDateString());
                    Console.WriteLine("{0}\t        {1}", deposi[i], balance[i]);
                }
            }
            //accepting an account number to view details
            Console.Write("\nEnter the account number whose details you want to view: ");
            choice = Convert.ToInt32(Console.ReadLine());

            //displaying the account details according to the choice entered using break statement
            for (i = 0; i < accountNumber.Length; i++)
            {
                if (accountNumber[i] == choice)
                {
                    found = true;
                    Console.WriteLine("Record Found!");
                    Console.WriteLine("Id\tA/c No.\tA/c Type  Opening Date\tDeposit($)   Balance($)");
                    Console.WriteLine("{0}\t{1} \t", customerId[i], accountNumber[i]);
                    Console.Write("{0}\t   {1}\t", accountType[i], dateOpened[i].ToShortDateString());
                    Console.WriteLine("{0}\t      {1}", deposi[i], balance[i]);
                    break;
                }
            }
            if (!found)
                Console.WriteLine("Account dose not exist.");

        }

    }
}
